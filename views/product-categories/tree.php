<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'Дерево категорий';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-about">
    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= $product_categories_tree ?>
    </p>

    <code><?= __FILE__ ?></code>
</div>
