<?php

use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\models\Products */

$this->title = 'Update Products: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Древо категорий', 'url' => Url::to(['product-categories/tree'])];
$this->params['breadcrumbs'][] = ['label' => $category_title, 'url' => Url::to(['index', 'category_id' => $model->category_id])];
$this->params['breadcrumbs'][] = ['label' => $model->name.' (view)', 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Редактирование записи';
?>
<div class="products-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
