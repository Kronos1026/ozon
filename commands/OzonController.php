<?php

namespace app\commands;

use yii\console\Controller;


class OzonController extends Controller
{
	public function actionIndex()
    {
		echo "Actions:\n";
		echo "ozon/index - display commends list\n";
		echo "ozon/parse - parsing from http://www.ozon.ru/context/partner_xml/\n";
    }
	
    public function actionParse()
    {
		\app\models\Utils::parseOzon();
    }
}
