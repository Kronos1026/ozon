<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "product_categories".
 *
 * @property integer $id
 * @property string $title
 * @property integer $created_at
 * @property integer $updated_at
 */
class ProductCategories extends \yii\db\ActiveRecord
{
	const STATUS_CHECKED_DEFAULT = 0;
	const STATUS_CHECKED = 1;
	
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'product_categories';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'parent_id'], 'required'],
            [['created_at', 'updated_at', 'parent_id'], 'integer'],
            [['title'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
			'parent_id' => 'Parent ID',
        ];
    }
	
	public function behaviors()
	{
		return [
			TimestampBehavior::className(),
		];
	}
	
	public static function getProductCategories() {
		$products_count = (new \yii\db\Query())
			->select(['COUNT(*) as products_count', 'category_id'])
			->from(Products::tableName())
			->groupBy(['category_id'])
			->createCommand()
			->sql;
		
		$product_categories = (new \yii\db\Query())
			->select(['pc.id', 'pc.title', 'pc.parent_id', 'p.products_count'])
			->from(['pc' => ProductCategories::tableName()])
			->leftJoin(['p' => '('.$products_count.')'], 'p.category_id = pc.id')
			->all();
		
		return $product_categories;
	}
}
