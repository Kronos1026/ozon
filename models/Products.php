<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "products".
 *
 * @property integer $id
 * @property string $name
 * @property string $url
 * @property double $price
 * @property string $currency_id
 * @property integer $category_id
 * @property integer $checked_status
 * @property integer $main_category_id
 * @property integer $created_at
 * @property integer $updated_at
 */
class Products extends \yii\db\ActiveRecord
{
	const STATUS_CHECKED_DEFAULT = 0;
	const STATUS_CHECKED = 1;
	
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'products';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['price'], 'number'],
            [['category_id', 'checked_status', 'main_category_id'], 'required'],
            [['category_id', 'checked_status', 'main_category_id', 'created_at', 'updated_at'], 'integer'],
            [['name', 'url', 'currency_id'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'url' => 'Url',
            'price' => 'Price',
            'currency_id' => 'Currency ID',
            'category_id' => 'Category ID',
            'checked_status' => 'Checked Status',
            'main_category_id' => 'Main Category ID',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
	
	public function behaviors()
	{
		return [
			TimestampBehavior::className(),
		];
	}
}
