<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\helpers\Url;

/**
 * ContactForm is the model behind the contact form.
 */
class Utils extends Model
{
	public static function getOzonFilesFolder() {
		return Yii::getAlias('@app').DIRECTORY_SEPARATOR.Yii::$app->params['ozon_files_folder'];
	}
	
	public static function getOzonZipFileServerPathFromUrl($file_url) {
		return (static::getOzonFilesFolder()).DIRECTORY_SEPARATOR.(basename($file_url));
	}
	
	public static function getExtractOzonZipFileOnServer($zip_file_path) {
		$zip = new \ZipArchive;
		$zip->open($zip_file_path);
		$zip->extractTo(static::getOzonFilesFolder());
		$zip->close();
		
		return (static::getOzonFilesFolder()).DIRECTORY_SEPARATOR.basename($zip_file_path, '.zip').'.xml';
	}
	
	public static function parseProductCategories($xml_file_path, $main_category_id) {
		$OzonParsingLogger = OzonParsingLogger::getLastLogger();
		$OzonParsingLogger->parsing_object_type = OzonParsingLogger::PARSING_OBJECT_CATEGORY;
		$OzonParsingLogger->save();
		
		$reader = new \XMLReader();
		$reader->open($xml_file_path); 
		
		while($reader->read() && $reader->name !== 'category');
		
		if ($OzonParsingLogger->status == OzonParsingLogger::STATUS_RESTARTED) {
			while($reader->read() && $reader->name !== 'category' && $reader->getAttribute('id') != $OzonParsingLogger->parsing_item_id);
		}
		
		$max_count_categories_for_parsing = Yii::$app->params['max_count_categories_for_parsing'];
		
		$OzonParsingLogger->current_main_category_id = $main_category_id;
		$OzonParsingLogger->save();
		
		while($reader->name === 'category'){
			$category_xml_info = new \SimpleXMLElement($reader->readOuterXML());
			$category_id = intval($category_xml_info['id']);
			$category_title = (string)$category_xml_info;
			$category_parent_id = intval($category_xml_info['parentId']);
			
			if ($max_count_categories_for_parsing > 0) {
				$parent_exists = ProductCategories::find()->where([
					'id' => $category_parent_id, 
					'checked_status' => ProductCategories::STATUS_CHECKED
				])->count() > 0 ? true : false;
				$current_count_categories = ProductCategories::find()->where([
					'parent_id' => $category_parent_id, 
					'checked_status' => ProductCategories::STATUS_CHECKED
				])->count();
				if ($category_parent_id != 0 && ($current_count_categories >= $max_count_categories_for_parsing || !$parent_exists)) {
					$reader->next('category');
					$OzonParsingLogger->parsing_item_id = $category_id;
					$OzonParsingLogger->save();
					continue;
				}
			}
			
			$category = ProductCategories::find()->where(['id' => $category_id])->one();
		
			if ($category === null) {
				$category = new ProductCategories();
				$category->id = $category_id;
			}
			
			$category->title = $category_title;
			$category->parent_id = $category_parent_id;
			$category->checked_status = ProductCategories::STATUS_CHECKED;
			$category->main_category_id = $main_category_id;
			$category->save();
			$OzonParsingLogger->parsing_item_id = $category_id;
			$OzonParsingLogger->save();
			
			$reader->next('category');
		}   
		ProductCategories::deleteAll([
			'and', 
			['checked_status' => ProductCategories::STATUS_CHECKED_DEFAULT], 
			['main_category_id' => $main_category_id]
		]);
		ProductCategories::updateAll(
			['checked_status' => ProductCategories::STATUS_CHECKED_DEFAULT], 
			['main_category_id' => $main_category_id]
		); 
		
		$reader->close();
	}
	
	public static function parseProducts($xml_file_path, $main_category_id) {
		$OzonParsingLogger = OzonParsingLogger::getLastLogger();
		$OzonParsingLogger->parsing_object_type = OzonParsingLogger::PARSING_OBJECT_PRODUCT;
		$OzonParsingLogger->save();
		
		$reader = new \XMLReader();
		$reader->open($xml_file_path); 
		
		while($reader->read() && $reader->name !== 'offer');
		
		if ($OzonParsingLogger->status == OzonParsingLogger::STATUS_RESTARTED) {
			while($reader->read() && $reader->name !== 'offer' && $reader->getAttribute('id') != $OzonParsingLogger->parsing_item_id);
		}
		
		$max_count_products_for_parsing = Yii::$app->params['max_count_products_for_parsing'];
		while($reader->name === 'offer'){
			$product_xml_info = new \SimpleXMLElement($reader->readOuterXML());
			$product_id = intval($product_xml_info['id']);
			$product_name = (string)$product_xml_info->name;
			$product_url = (string)$product_xml_info->url;
			$product_price = floatval($product_xml_info->price);
			$product_currency_id = (string)$product_xml_info->currencyId;
			$product_category_id = intval($product_xml_info->categoryId);
			
			if ($max_count_products_for_parsing > 0) {
				$category_exists = ProductCategories::find()->where(['id' => $product_category_id])->count() > 0 ? true : false;
				$current_count_products = Products::find()->where([
					'category_id' => $product_category_id, 
					'checked_status' => Products::STATUS_CHECKED
				])->count();
				if ($current_count_products >= $max_count_products_for_parsing || !$category_exists) {
					$reader->next('offer');
					$OzonParsingLogger->parsing_item_id = $product_id;
					$OzonParsingLogger->save();
					continue;
				}
			}
			
			$product = Products::find()->where(['id' => $product_id])->one();
		
			if ($product === null) {
				$product = new Products();
				$product->id = $product_id;
			}
			
			$product->name = $product_name;
			$product->url = $product_url;
			$product->price = $product_price;
			$product->currency_id = $product_currency_id;
			$product->category_id = $product_category_id;
			$product->checked_status = Products::STATUS_CHECKED;
			$product->main_category_id = $main_category_id;
			$product->save();
			
			$OzonParsingLogger->parsing_item_id = $product_id;
			$OzonParsingLogger->save();
			
			$reader->next('offer');
		}
		Products::deleteAll(['and', ['checked_status' => Products::STATUS_CHECKED_DEFAULT], ['main_category_id' => $main_category_id]]);
		Products::updateAll(['checked_status' => Products::STATUS_CHECKED_DEFAULT], ['main_category_id' => $main_category_id]);		
		
		$reader->close();
	}
	
	public static function parseOzonXMLFile($xml_file_path, $main_category_id) {
		$OzonParsingLogger = OzonParsingLogger::getLastLogger();
		
		//if status reparse products from some category
		if ($OzonParsingLogger->status == OzonParsingLogger::STATUS_RESTARTED && $OzonParsingLogger->parsing_object_type == OzonParsingLogger::PARSING_OBJECT_PRODUCT) {
			echo "Parsing products...<br>\n";
			static::parseProducts($xml_file_path, $main_category_id);
		} else {
			echo "Parsing categories...<br>\n";
			static::parseProductCategories($xml_file_path, $main_category_id);
			echo "Parsing products...<br>\n";
			static::parseProducts($xml_file_path, $main_category_id);
		}
		//continue simple parsing
		$OzonParsingLogger->status = OzonParsingLogger::STATUS_NOT_COMPLETED;
		$OzonParsingLogger->save();
	}
	
	
	public static function printOzonCategoryTree($product_categories, $parent_id = 0, $lvl = 0) {
		$html = '';
		if (count($product_categories)) {
			foreach ($product_categories as $product_category) {
				if ($product_category['parent_id'] == $parent_id) {
					$products_count = intval($product_category['products_count']);
					$html_item = $product_category['title'];
					if ($products_count) {
						$html_item .= " ($products_count)";
						$html_item = '<a href="'.Url::to(['products/index', 'category_id' => $product_category['id']]).'">'.$html_item.'</a>';
					}
					$html_item = str_repeat('----', $lvl).$html_item.'<br>';
					$html .= $html_item;
					$html .= static::printOzonCategoryTree($product_categories, $product_category['id'], $lvl + 1);
				}
			}
		}
		
		return $html;
	}
	
	public static function parseOzon() {
		$url_list_main_product_categories_files = Yii::$app->params['url_list_main_product_categories_files'];
		
		$start_index = 0;
		$main_categories_ids = array_column($url_list_main_product_categories_files, 'id');
		
		$OzonParsingLogger = OzonParsingLogger::getLastLogger();
		if ($OzonParsingLogger === null || $OzonParsingLogger->status == OzonParsingLogger::STATUS_COMPLETED) {
			$OzonParsingLogger = new OzonParsingLogger();
			$OzonParsingLogger->status = OzonParsingLogger::STATUS_NOT_COMPLETED;
			$OzonParsingLogger->save();
		} else {
			echo "The last parsing was not completed.<br>\n";
			$start_index = array_search($OzonParsingLogger->current_main_category_id, $main_categories_ids);
			if ($start_index !== false) {
				echo "Continue parsing from the last point...<br><br>\n\n";
				$OzonParsingLogger->status = OzonParsingLogger::STATUS_RESTARTED;
				$OzonParsingLogger->save();
			} else {
				echo "Cannot find last category point in config. Starting a new parsing...<br><br>\n\n";
				$OzonParsingLogger->status = OzonParsingLogger::STATUS_COMPLETED;
				$OzonParsingLogger->save();
				$OzonParsingLogger = new OzonParsingLogger();
				$OzonParsingLogger->status = OzonParsingLogger::STATUS_NOT_COMPLETED;
				$OzonParsingLogger->save();
				$start_index = 0;
			}
		}
		
		for ($file_num = $start_index; $file_num < count($url_list_main_product_categories_files); $file_num++) {
			$main_category_id = $url_list_main_product_categories_files[$file_num]['id'];
			$url_main_product_category_file = $url_list_main_product_categories_files[$file_num]['url'];
			
			$start = microtime(true);
			$OzonParsingLogger->url = $url_main_product_category_file;
			$OzonParsingLogger->save();
			echo 'STARTED: ['.($file_num + 1).'/'.count($url_list_main_product_categories_files).']<br>'."\n";
			$ozon_main_product_category_zip_file_server_path = Utils::getOzonZipFileServerPathFromUrl($url_main_product_category_file);
			if(!@copy($url_main_product_category_file, $ozon_main_product_category_zip_file_server_path))
			{
				$errors = error_get_last();
				echo "COPY ERROR: ".$errors['type']."<br />\n".$errors['message'];
			} else {
				$ozon_main_product_category_xml_file_server_path = Utils::getExtractOzonZipFileOnServer($ozon_main_product_category_zip_file_server_path);
				unlink($ozon_main_product_category_zip_file_server_path);
				
				Utils::parseOzonXMLFile($ozon_main_product_category_xml_file_server_path, $main_category_id);
				unlink($ozon_main_product_category_xml_file_server_path);
			}
			echo 'COMPLETED: ['.($file_num + 1).'/'.count($url_list_main_product_categories_files).'] (time: '.(microtime(true) - $start).' s)<br><br>'."\n\n";
		}
		
		$OzonParsingLogger = OzonParsingLogger::getLastLogger();
		if (count($main_categories_ids)) {
			Products::deleteAll(['NOT IN', 'main_category_id', $main_categories_ids]);
			ProductCategories::deleteAll(['NOT IN', 'main_category_id', $main_categories_ids]);
		} else {
			Products::deleteAll();
			ProductCategories::deleteAll();
		}
		$OzonParsingLogger->status = OzonParsingLogger::STATUS_COMPLETED;
		$OzonParsingLogger->save();
		echo "Parsing DONE<br><br>\n\n";
	}
}
