<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "ozon_parsing_logger".
 *
 * @property integer $id
 * @property integer $current_main_category_id
 * @property string $url
 * @property integer $parsing_object_type
 * @property integer $parsing_item_id
 * @property integer $status
 * @property string $main_categories_json
 * @property integer $created_at
 * @property integer $updated_at
 */
class OzonParsingLogger extends \yii\db\ActiveRecord
{
	const STATUS_NOT_COMPLETED = 0;
	const STATUS_COMPLETED = 1;
	const STATUS_RESTARTED = 2;
	
	const PARSING_OBJECT_CATEGORY = 1;
	const PARSING_OBJECT_PRODUCT = 2;
	
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ozon_parsing_logger';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['current_main_category_id', 'parsing_object_type', 'parsing_item_id', 'status', 'created_at', 'updated_at'], 'integer'],
            [['url'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'current_main_category_id' => 'Current Main Category ID',
            'url' => 'Url',
            'parsing_object_type' => 'Parsing Object Type',
            'parsing_item_id' => 'Parsing Item ID',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
	
	public function behaviors()
	{
		return [
			TimestampBehavior::className(),
		];
	}
	
	public static function getLastLogger() {
		return self::find()->orderBy(['id' => SORT_DESC])->one();
	}
}
