<?php

return [
    'adminEmail' => 'admin@example.com',
	//List of main product categories files
	'url_list_main_product_categories_files' => [
		['id' => 1132530, 'url' => 'http://static.ozone.ru/multimedia/feeds/facet/div_tech/1132530.zip'], //Электроника
		['id' => 1133722, 'url' => 'http://static.ozone.ru/multimedia/feeds/facet/div_appliance/1133722.zip'], //Бытовая техника
		/*['id' => 1145470, 'url' => 'http://static.ozone.ru/multimedia/feeds/facet/div_home/1145470.zip'], //Дом и сад
		['id' => 1147991, 'url' => 'http://static.ozone.ru/multimedia/feeds/facet/div_kid/1147991.zip'], //Детям и мамам
		['id' => 1160574, 'url' => 'http://static.ozone.ru/multimedia/feeds/facet/div_writing/1160574.zip'], //Канцелярские товары
		['id' => 1158309, 'url' => 'http://static.ozone.ru/multimedia/feeds/facet/div_beauty/1158309.zip'], //Красота и здоровье
		['id' => 1145489, 'url' => 'http://static.ozone.ru/multimedia/feeds/facet/div_bs/1145489.zip'], //Спорт и отдых
		['id' => 1153308, 'url' => 'http://static.ozone.ru/multimedia/feeds/facet/div_fashion/clothing.zip'], //Одежда
		['id' => 1180053, 'url' => 'http://static.ozone.ru/multimedia/feeds/facet/div_food/1180053.zip'], //Продукты питания
		['id' => 1145472, 'url' => 'http://static.ozone.ru/multimedia/feeds/facet/div_animals/1145472.zip'], //Зоотовары
		['id' => 1132532, 'url' => 'http://static.ozone.ru/multimedia/feeds/facet/div_soft/1132532.zip'], //Игры и софт
		['id' => 1132528, 'url' => 'http://static.ozone.ru/multimedia/feeds/facet/div_dvd/1132528.zip'], //DVD и Blu-ray
		['id' => 1132529, 'url' => 'http://static.ozone.ru/multimedia/feeds/facet/div_music/1132529.zip'], //Музыка
		['id' => 1132527, 'url' => 'http://static.ozone.ru/multimedia/feeds/facet/div_rar/rar.zip'], //Антикварные книги
		['id' => 11325270, 'url' => 'http://static.ozone.ru/multimedia/feeds/facet/div_book/1132527.zip'], //Книги*/
	],
	//Count of categories for parsing
	'max_count_categories_for_parsing' => 2,
	//Count of products for parsing
	'max_count_products_for_parsing' => 2,
	//ozon files folder
	'ozon_files_folder' => 'ozon_files',
];
