<?php

use yii\db\Migration;

class m170406_195355_create_table_products extends Migration
{
    public function up()
    {
		$this->createTable('products', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
			'url' => $this->string(),
			'price' => $this->float(2),
			'currency_id' => $this->string(),
			'category_id' => $this->integer()->notNull(),
			'checked_status' => $this->integer()->notNull(),
			'main_category_id' => $this->integer()->notNull(),
			'created_at' => $this->integer()->notNull(),
			'updated_at' => $this->integer()->notNull(),
        ]);
    }

    public function down()
    {
        $this->dropTable('products');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
