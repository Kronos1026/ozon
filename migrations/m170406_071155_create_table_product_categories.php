<?php

use yii\db\Migration;

class m170406_071155_create_table_product_categories extends Migration
{
    public function up()
    {
		$this->createTable('product_categories', [
            'id' => $this->primaryKey(),
            'title' => $this->string()->notNull(),
			'parent_id' => $this->integer()->notNull(),
			'checked_status' => $this->integer()->notNull(),
			'main_category_id' => $this->integer()->notNull(),
			'created_at' => $this->integer()->notNull(),
			'updated_at' => $this->integer()->notNull(),
        ]);
    }

    public function down()
    {
		$this->dropTable('product_categories');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
