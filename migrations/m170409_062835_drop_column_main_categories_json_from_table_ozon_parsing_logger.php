<?php

use yii\db\Migration;

class m170409_062835_drop_column_main_categories_json_from_table_ozon_parsing_logger extends Migration
{
    public function up()
    {
		$this->dropColumn('ozon_parsing_logger', 'main_categories_json');
    }

    public function down()
    {
        echo "m170409_062835_drop_column_main_categories_json_from_table_ozon_parsing_logger cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
