<?php

use yii\db\Migration;

class m170407_083126_create_table_ozon_parsing_logger extends Migration
{
    public function up()
    {
		$this->createTable('ozon_parsing_logger', [
            'id' => $this->primaryKey(),
            'current_main_category_id' => $this->integer(),
			'url' => $this->string(),
			'parsing_object_type' => $this->integer(1),
			'parsing_item_id' => $this->integer(),
			'status' => $this->integer(),
			'main_categories_json' => $this->text(),
			
			'created_at' => $this->integer()->notNull(),
			'updated_at' => $this->integer()->notNull(),
        ]);
    }

    public function down()
    {
        $this->dropTable('ozon_parsing_logger');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
